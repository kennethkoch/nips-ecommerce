from django.contrib import admin
from nips_web.models import Dimensions, Product, Category

# Register your models here.

admin.site.register(Product)
admin.site.register(Dimensions)
admin.site.register(Category)
