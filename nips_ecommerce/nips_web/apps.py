from django.apps import AppConfig


class NipsWebConfig(AppConfig):
    name = 'nips_web'
