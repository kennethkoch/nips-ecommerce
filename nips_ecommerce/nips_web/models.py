from django.db import models

# Create your models here.

class Dimensions(models.Model):
    l = models.FloatField(default=0.0)
    h = models.FloatField(default=0.0)
    d = models.FloatField(default=0.0)

    def __str__(self):
        return 'l=' + str(self.l) + 'x' + 'h=' + str(self.h) + 'x' + 'd=' + str(self.d)

class Category(models.Model):
    name = models.CharField(max_length=20)
    class Meta:
        verbose_name_plural = 'categories'

    def __str__(self):
        return self.name

class Product(models.Model):
    name = models.CharField(max_length=20)
    quantity = models.PositiveIntegerField(default=0)
    info = models.TextField(null=True, blank=True)
    price = models.DecimalField(max_digits=10, decimal_places=2, null=True, blank=True)
    arriving_date = models.DateField(auto_now_add=True)
    material = models.TextField(null=True, blank=True)
    colour = models.CharField(max_length=10)
    dimensions = models.ForeignKey(Dimensions, on_delete=models.DO_NOTHING, null=True, blank=True)
    category = models.ForeignKey(Category, on_delete=models.DO_NOTHING, null=True, blank=True)

    def __str__(self):
        return self.name
