from django.urls import path, include
from nips_web.views import HomeView, ProductListView, DetailProductView

urlpatterns = [
    path('', HomeView.as_view(), name='home'),
    path('list/', ProductListView.as_view(), name="productlist"),
    path('product/<int:pk>', DetailProductView.as_view(), name="detail")
    ]
