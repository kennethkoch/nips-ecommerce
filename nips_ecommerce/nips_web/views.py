from django.shortcuts import render
from django.http import HttpResponse
from django.views.generic import TemplateView, ListView, CreateView, UpdateView, DeleteView, DetailView
from nips_web.models import Product
from django.urls import reverse_lazy

class HomeView(TemplateView):
    template_name = "home.html"

class ProductListView(ListView):
    model = Product
    template_name = "productlist.html"
    context_object_name = "products"

class DetailProductView(DetailView):
    model = Product
    template_name = 'productdetail.html'
    context_object_name = 'product'
    fields = '__all__'
    success_url = reverse_lazy('list')